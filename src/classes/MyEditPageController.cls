public class MyEditPageController {
    public Contact con{get;set;}
    public MyEditPageController() {
        String ContactId=ApexPages.currentPage().getParameters().get('cId');
		if( ContactId != null ) {
		con = new Contact();
		con = [Select Name,Phone,Department,Email from Contact where Id = :ContactId];
    	}  else {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'No Contact Id Found'));
		}

	}
    public PageReference saveContact() {
		if(con != null) {
			update con;
            PageReference myPage = new PageReference('/apex/MyPage?cid='+con.Id+'&confirmmsg=Successfully Edited');  
        	return myPage;
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'No Contact Record Found')); 
        	return null;
        }
		
    }
    
    public PageReference cancelContact() {
        if(con != null) {
            PageReference myPage = new PageReference('/apex/MyPage');  
        	return myPage;
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'No Contact Record Found')); 
        	return null;
        }
    }
}