@isTest
private class ClassTriggerTest {
    @isTest static void deleteClassWithStudent() {
        Class__c getclass = UtilityClass.getclass(4,true);
        Student1__c student = new Student1__c(Gender__c = 'Female' , Class__c = getclass.Id);
        insert student;
        
        Test.startTest();
        Database.DeleteResult result = Database.delete(getclass, false);
        Test.stopTest();
        System.assert(!result.isSuccess());
    }
    @isTest static void deleteClassWithoutStudent() {
        Class__c getclass = UtilityClass.getclass(4,true);
        Test.startTest();
        Database.DeleteResult result = Database.delete(getclass, false);
        Test.stopTest();
        System.assert(result.isSuccess());
    }
}