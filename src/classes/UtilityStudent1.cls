public class UtilityStudent1 {
    public static Class__c getclass(Integer n , Boolean b) {
        Class__c cls = new Class__c(Max_Count__c = n ,Count__c = 0);
        if(b) {
            insert cls;
        }
        return cls;
    }
    public static List<Student1__c> getstudent(Integer n ,String gender ,Id classid ,Boolean b) {
        List<Student1__c> stdlist = new List<Student1__c>();
        for(Integer i =0 ; i < n ;i++) {
        	Student1__c std = new Student1__c(Gender__c = gender , Class__c = classid);
            stdlist.add(std);
        }
        if(b) {
            insert stdlist;
        }
        return stdlist;
    }
}