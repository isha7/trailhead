global class SendMailToManagerBatch implements Database.Batchable<sObject> {
    public Date day;
    public Date firstDay;
    public Date lastDay;
    global SendMailToManagerBatch() {
        day = Date.today();
        firstDay = Date.newInstance(day.year(), day.month(), 1);
        Integer monthinterval =(Integer) NotificationPeriod__c.getInstance('notification time interval').MonthInterval__c;
        lastDay = FirstDay.addMonths(monthinterval).toStartofMonth().addDays(-1);
        System.Debug('.........../...../...');
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        System.Debug('.........../...../...');
        String query = 'SELECT FirstYearCompletionDate__c,Manager__c,Manager__r.First_Name__c,Manager__r.Email__c,Name,Last_Promotion_Date__c ,LastPromotionCompletion__c FROM Employee__c WHERE (FirstYearCompletionDate__c >= :firstDay AND FirstYearCompletionDate__c <= :lastDay) OR LastPromotionCompletion__c <= :lastDay';
        List<Employee__c> l = database.query(query);
        System.debug('>>>>>>>>>'+l);
        
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<Employee__c> records) {
        Map<Id,Employee__c> managerIdtoEmailMap = new Map<Id,Employee__c>();
        Map<Id,String> managerToPrintMap = new Map<Id,String>();
        for(Employee__c emp : records) {
            if(!managerIdtoEmailMap.containsKey(emp.Manager__c)) {
                managerIdtoEmailMap.put(emp.Manager__c,emp);
            }
            if(!managerToPrintMap.containsKey(emp.Manager__c)) {
                managerToPrintMap.put(emp.Manager__c,'');
            }
            if(emp.FirstYearCompletionDate__c >= firstDay && emp.FirstYearCompletionDate__c <= lastDay){
                if(managerToPrintMap.containsKey(emp.Manager__c)) {
                    managerToPrintMap.put(emp.Manager__c,managerToPrintMap.get(emp.Manager__c) +' <br/> <b> '+emp.Name+'</b>'+' '+'has completed 1 year');
                }
            }
            if(emp.Last_Promotion_Date__c == Null) {
                if(managerToPrintMap.containsKey(emp.Manager__c)) {
                    managerToPrintMap.put(emp.Manager__c,managerToPrintMap.get(emp.Manager__c) +' <br/> <b> '+emp.Name+'</b>'+' '+' has never promoted.');
                }
            }
            if(emp.LastPromotionCompletion__c >= firstDay && emp.LastPromotionCompletion__c <= lastDay) {
                if(managerToPrintMap.containsKey(emp.Manager__c)) {
                    managerToPrintMap.put(emp.Manager__c,managerToPrintMap.get(emp.Manager__c) +'  <br/> <b> '+emp.Name+'</b>'+' '+' will going to have next promotion.');
                }
            }
            if(emp.LastPromotionCompletion__c < firstDay) {
                if(managerToPrintMap.containsKey(emp.Manager__c)) {
                    managerToPrintMap.put(emp.Manager__c,managerToPrintMap.get(emp.Manager__c) +' <br/> <b>  '+emp.Name+'</b>'+' '+' has due promotion.');
                }
            }
        }
        EmailTemplate emailTemplateRecord = [SELECT Id,HtmlValue From EmailTemplate WHERE Name = 'notify'];
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
        for(Id manId : managerToPrintMap.keySet()) {
            String htmlBody = emailTemplateRecord.HtmlValue;
            htmlBody = htmlBody.replaceAll('<!\\[CDATA\\[', ''); // replace '<![CDATA['
            htmlBody= htmlBody.replaceAll('\\]\\]>', ''); // replace ']]'
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSubject('Employee Details');
            htmlBody = htmlBody.replace('hello','Hello'+' '+ managerIdtoEmailMap.get(manId).Manager__r.First_Name__c+',<br/>'+managerToPrintMap.get(manId)+'<br/><br/> HR Manager<br/>(Appirio, India)');
            mail.setHtmlBody(htmlBody);
            List<String> sendTo = new List<String>();
            sendTo.add(managerIdtoEmailMap.get(manId).Manager__r.Email__c);
            mail.setToAddresses(sendTo);
            allmsg.add(mail);
        }
        Messaging.sendEmail(allmsg);
        System.debug(' managerToPrintMap=====>>>'+ managerToPrintMap);
    }
    global void finish(Database.BatchableContext bc) {
    }
}