@isTest
public class AnimalLocatorTest {
    @isTest static void getAnimalName() {
        Test.SetMock(HttpCallOutMock.class,new AnimalLocatorMock());
        String result1 = AnimalLocator.getAnimalNameById(1);
        System.assertNotEquals(Null,result1);
        System.assertEquals(false, String.isEmpty(result1));
        System.assertEquals('chicken',result1);
    }
}