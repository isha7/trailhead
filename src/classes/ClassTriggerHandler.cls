public class ClassTriggerHandler {
    //prevent deletion of class if  there is at least 1 student associated with class.
    public static void beforeDelete(Map<id,Class__c> oldTriggerMap) {
        for(Class__c cid : [SELECT Id,(SELECT Class__c FROM ourStudent1s__r) FROM Class__c WHERE Id IN :oldTriggerMap.keyset()]) {
            if(cid.ourStudent1s__r.size() > 0)   {                  
            	oldTriggerMap.get(cid.id).addError('It has Student associated');
             }
        }
    }
    
    public static void onChangeCount(List<Class__c> newTriggerList , Map<Id,Class__c> oldTriggerMap) { 
        List<Class__c> listofclassid = new List<Class__c>();
   
        for(Class__c classid : newTriggerList) {
            if(classid.Count__c != oldTriggerMap.get(classid.Id).Count__c  &&  classid.Count__c == 0)  {
                listofclassid.add(classid);
            }
         }
        
        List<Student1__c> studentlist = new LIST<Student1__c>([SELECT Id FROM Student1__c WHERE Class__c IN :listofclassid]); 
        delete studentlist;
        //List<Student1__c> studentlistformaxcount = new LIST<Student1__c>([SELECT Id FROM Student1__c WHERE Class__c IN :listformaxcount]); 
        
        
    }
    public static void deleteStudent(List<Class__c> oldlist) {
        Set<Id> ids = new Set<Id>();
        for(Class__c c : oldlist) {
            ids.add(c.Id);
        }
         List<Student1__c> studentlist = [SELECT Class__c,id FROM Student1__c WHERE Class__c IN :ids];
    	 delete studentlist;
    }
}