global class PromotionNotifyManager implements Database.Batchable<sObject> {
    public Date day;
    public Date firstDay;
    public Date lastDay;
    global PromotionNotifyManager() {
      	day = Date.today();
        firstDay = Date.newInstance(day.year(), day.month(), 1);
        lastDay = FirstDay.addMonths(1).toStartofMonth().addDays(-1);
    }	
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Manager__c,Manager__r.Email__c,First_Name__c,Last_Name__c,Last_Promotion_Date__c FROM Employee__c WHERE LastPromotionCompletion__c <= :lastDay';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<Employee__c> records) {
        Map<Id,String> mapManagertoEmployeeThisMonth = new Map<Id,String>();
        Map<Id,String> mapManagertoEmployeePreviousMonth = new Map<Id,String>();
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        String s = '';
        String st = '';
        Map<Id,Employee__c> managerIdtoMailMap = new Map<Id,Employee__c>();
        for(Employee__c m : records) {
            if(!managerIdtoMailMap.containskey(m.Manager__c)){
                managerIdtoMailMap.put(m.Manager__c,m);
            }
            if(m.LastPromotionCompletion__c >= firstDay && m.LastPromotionCompletion__c <= lastDay) {
                if(!mapManagertoEmployeeThisMonth.containsKey(m.Manager__c)) {
                    mapManagertoEmployeeThisMonth.put(m.Manager__c,'');
                    s=s + ' '+m.First_Name__c +''+m.Last_Name__c+' ';
                    mapManagertoEmployeeThisMonth.put(m.Manager__c ,s);
                } else {
                    s=s + ' '+m.First_Name__c +''+m.Last_Name__c+' ';
                    mapManagertoEmployeeThisMonth.put(m.Manager__c ,s);
                }
            } else if(m.LastPromotionCompletion__c < firstDay) {
                if(!mapManagertoEmployeePreviousMonth.containsKey(m.Manager__c)) {
                    mapManagertoEmployeePreviousMonth.put(m.Manager__c,'');
                    st=st + ' '+m.First_Name__c +''+m.Last_Name__c+' ';
                    mapManagertoEmployeePreviousMonth.put(m.Manager__c ,st);
                } else {
                    st=st + ' '+m.First_Name__c +''+m.Last_Name__c+' ';
                    mapManagertoEmployeePreviousMonth.put(m.Manager__c ,st);
                }
            }
        } 
        for(Id m : managerIdtoMailMap.keySet()) {
            List<String> toAddresses = new List<String>();           
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSubject('Promotion of Employees');
            String messageBody = '';
            if(mapManagertoEmployeeThisMonth != Null && mapManagertoEmployeeThisMonth.containsKey(m)) {
                toAddresses.add(managerIdtoMailMap.get(m).Manager__r.Email__c);
                mail.setToAddresses(toAddresses);
                messageBody = messageBody + 'Hi, Your Employees - ' +mapManagertoEmployeeThisMonth.get(m) + 'was promoted last year now its time for their next promotion';
            }
            if(mapManagertoEmployeePreviousMonth != Null && mapManagertoEmployeePreviousMonth.containsKey(m)) {
                toAddresses.add(managerIdtoMailMap.get(m).Manager__r.Email__c);
                mail.setToAddresses(toAddresses);
                messageBody = messageBody +'Hi, Your Employees - ' +mapManagertoEmployeePreviousMonth.get(m) + 'was not promoted yet';
            }
            mail.setHtmlBody(messageBody); 
            mailList.add(mail);
        }
        Messaging.sendEmail(mailList);
    }
    global void finish(Database.BatchableContext bc) {
    }
}