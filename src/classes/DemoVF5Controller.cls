public class DemoVF5Controller {
	public Account acc{get;set;}
	public User u{get;set;}
	public DemoVF5Controller() {
		String AccountId=ApexPages.currentPage().getParameters().get('aId');
		if(AccountId!=null) {
		acc = new Account();
		acc = [Select name,phone from Account where Id =:AccountId];
		} else {
			acc = new Account();
		}
	}
	public PageReference save(){
		if(acc.Id !=null) {
			update acc;
		} else {
			insert acc;
		}
	    PageReference homePage = new PageReference('/apex/DemoVF3?id='+acc.Id);  
        homepage.setRedirect(true);
        return homePage;
	}
	public PageReference cancel() {
			 PageReference homePage = new PageReference('/apex/DemoVF1');  
        	 homepage.setRedirect(true);
        	return homePage;
	}
}