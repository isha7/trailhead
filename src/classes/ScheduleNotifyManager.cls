global class ScheduleNotifyManager implements Schedulable {
    global void execute(SchedulableContext sc) {
        NotifyManager nm = new NotifyManager();
        Database.executeBatch(nm);
    }
}