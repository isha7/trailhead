global class DemoBatch implements Database.Batchable<sObject>
{
    String query;
    String email;
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        query = 'select Name from Account';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Account> records)
    {
        List<Account> acclist = new List<Account>();
        for(Account acc : records) {
            acc.Phone = '1234';
            acclist.add(acc);
        }
        update acclist;
    }
    global void finish(Database.BatchableContext BC)
    {
        
    }
}