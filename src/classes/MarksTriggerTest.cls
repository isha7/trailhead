@isTest
private class MarksTriggerTest {
    @isTest static void checkMarks() {
        List<Contact> contactList = UtilityMarks.getContact('Last' , 1 , false);
        insert contactList;
        List<Marks__c> markList = new List<Marks__c>();
        Marks__c mark1 = new Marks__c(Subject__c = 'Maths' , Contact__c = contactList[0].Id, Marks_Obtained__c =20);
        Marks__c mark2 = new Marks__c(Subject__c = 'Science' , Contact__c = contactList[0].Id, Marks_Obtained__c =30);
        markList.add(mark1);
        markList.add(mark2);
        
        Test.startTest();
        insert markList;        
        System.assertEquals(50,[SELECT Total_Marks__c FROM Contact WHERE Id = :contactList[0].Id].Total_Marks__c);
        System.assertEquals(25,[SELECT Average_Marks__c FROM Contact WHERE Id = :contactList[0].Id].Average_Marks__c);
        Test.stopTest();
    }
}