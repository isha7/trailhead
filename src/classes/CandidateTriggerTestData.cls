public class CandidateTriggerTestData {
    public static List<Candidate__c> testData(Integer n, Boolean j) {
        List<Candidate__c> clist = new List<Candidate__c>();
        for(Integer i=1 ; i <= n ; i++ ) {
            Candidate__c c = new Candidate__c(First_Name__c = 'test'+i , Last_Name__c='last'+i , Email__c='test'+i+'@email.com');
            clist.add(c);
        }
        if(j) {
        insert clist;
    }
        return clist;
   }
}