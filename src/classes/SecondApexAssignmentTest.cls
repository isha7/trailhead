@isTest
public class SecondApexAssignmentTest {
    @isTest static void checkSaturday() {
        Date d1 = date.parse('3/26/2016');
        Date d2 = date.parse('3/28/2016');
        System.assertEquals(d2,SecondApexAssignment.getNextWorkingdayDate(d1));
    }
    @isTest static void checkFriday() {
        Date d1 = date.parse('3/25/2016');
        Date d2 = date.parse('3/28/2016');
        System.assertEquals(d2,SecondApexAssignment.getNextWorkingdayDate(d1));
    }
    @isTest static void checkElse() {
        Date d1 = date.parse('3/23/2016');
        Date d2 = date.parse('3/24/2016');
        System.assertEquals(d2,SecondApexAssignment.getNextWorkingdayDate(d1));
    }
    @isTest static void getteachers() {
        Class__c cls = UtilitySecondApexAssignment.getclass(True);
        Teacher__c t = new Teacher__c(Name='Teachertest' ,Class__c = cls.Id);
        insert t;
        List<Teacher__c> gett = SecondApexAssignment.getTeacher(cls.Id);
        System.assertEquals(1, gett.size());
        
    }
    @isTest static void getnoteachers() {
        Class__c cls = UtilitySecondApexAssignment.getclass(True);
        List<Teacher__c> gett = SecondApexAssignment.getTeacher(cls.Id);
        System.assertEquals(0, gett.size());
    }
}