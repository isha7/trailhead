global class ScheduleSendMailToManagerBatch implements Schedulable {
    global void execute(SchedulableContext sc) {
        SendMailToManagerBatch obj = new SendMailToManagerBatch();
        Database.executeBatch(obj);
    }
}