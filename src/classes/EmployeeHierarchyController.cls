public class EmployeeHierarchyController {
    public Employee__c employee{get;set;}
    public Map<String,Employee__c> mapEmpToManager;
    public Set<Employee__c> hierarchySet;
    public Integer setsize{get;set;}
    public List<Employee__c> hierarchyList{get;set;}
    public List<Employee__c> hlist;
    //public Map<String,String> empToDesignMap{get;set;}
    public boolean show{get;set;}
    public EmployeeHierarchyController() {
        show=false;
        employee = new Employee__c();
        mapEmpToManager = new Map<String,Employee__c>();
        hlist = new List<Employee__c>();
        hierarchyList = new List<Employee__c>();
        hierarchySet =  new Set<Employee__c>();
        setsize = hierarchySet.size();
        //empToDesignMap = new Map<String,String>();
        List<Employee__c> empList = new List<Employee__c>([SELECT Id,Name,Manager__c,Email__c,Designation__c,Manager__r.Name FROM Employee__c]);
        
        system.debug(empList);
        for(Employee__c emp : empList) {
            if(emp.Manager__c != Null) {
               mapEmpToManager.put(emp.Id,emp);
            } else {
               mapEmpToManager.put(emp.Id,emp); 
            }
        } 
    }
    
    public void onSubmit() {
        hierarchySet.clear();
        hlist.clear();
        hierarchyList.clear();
        System.debug('hierarchySet=='+hierarchySet);
        System.debug('hierarchyList='+hierarchyList);
        show=false;
        createhierarchySet(employee.Manager__c);
        hlist.addAll(hierarchySet);
        System.debug('hierarchySetafter=='+hierarchySet);
        System.debug('hierarchyListafter='+hierarchyList);
        system.debug('hierarchySet.size() - 1 ='+(hierarchySet.size() - 1));
        for(Integer i = hlist.size()-1; i>=0;i--) {
            hierarchyList.add(hlist.get(i));
        }
        setsize = hierarchySet.size();
        System.debug('hierarchyListafter='+hierarchyList);
        show=true;
        system.debug('hierarchySetend=='+hierarchySet);
    }
    public void createhierarchySet(Id empId) {
        show=false;
        if(mapEmpToManager.containsKey(empId)) {
            if(mapEmpToManager.get(empId).Manager__c != Null) {
                hierarchySet.add(mapEmpToManager.get(empId));
                hierarchySet.add(mapEmpToManager.get(mapEmpToManager.get(empId).Manager__c));
                Id e = mapEmpToManager.get(empId).Manager__c;
                system.debug(e);
                createhierarchySet(e);
            } else if(mapEmpToManager.get(empId).Manager__c == Null) {
                hierarchySet.add(mapEmpToManager.get(empId));
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'No Employee with this Name Exists !'));
        }
    }
}