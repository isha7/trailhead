public class Student1TriggerHandler {
    //after insert... after update...after delete
    public static void onAfterInsertUpdateDelete(List<Student1__c> newTriggerList ,Map<Id,Student1__c> newTriggerMap, Map<Id,Student1__c> oldTriggerMap , List<Student1__c> oldTriggerList) {
        Set<Id> setofClassid = new Set<Id>();
        //insert case 
        if(newTriggerList != Null) {
        	for(Student1__c newlist : newTriggerList) {
                //update case
            	if(oldTriggerMap == Null || newlist.Class__c != oldTriggerMap.get(newlist.Id).Class__c || newlist.Gender__c != oldTriggerMap.get(newlist.Id).Gender__c) {
                	setofclassid.add(newlist.Class__c );
                	if(oldTriggerMap != Null) {
                    	setofclassid.add(oldTriggerMap.get(newlist.Id).Class__c);
                	}
            	}
        	}
        } else {
            //delete case
            if(oldTriggerList != Null) {
                for(Student1__c oldlist : oldTriggerList){
                    setofclassid.add(oldlist.Class__c);
                }
            }
        }
        List<Class__c> max = new List<Class__c>([SELECT Max_Count__c FROM Class__c WHERE Id IN :setofclassid]);
		Map<Id,Integer> mapClassidtoCount = new Map<Id,Integer>();
        Map<Id,Integer> mapClassidtoFemaleCount = new Map<Id,Integer>();
        Map<Id,Class__c> mapClassidtoMax = new Map<Id,Class__c>();
        
        for(Class__c cls : max) {
            if(!mapClassidtoMax.containskey(cls.Id)) {
                mapClassidtoMax.put(cls.Id,cls);
            }
        }
       
        
        List<Student1__c> existinglist = new List<Student1__c>([SELECT CLass__c ,Gender__c,Id FROM Student1__c WHERE CLass__c IN :setofclassid ORDER BY LastModifiedDate]);
        
        for(Student1__c std : existinglist) {
            //if count < maxcount-1 and creating increamenting count and no of female
            if((mapClassidtoCount.get(std.Class__c) < mapClassidtoMax.get(std.Class__c).Max_Count__c - 1) && (mapClassidtoCount.containskey(std.Class__c))) {
                mapClassidtoCount.put(std.Class__c,mapClassidtoCount.get(std.Class__c) + 1);
                if(std.Gender__c == 'Female' && std.Gender__c != Null) {
                    if(!mapClassidtoFemaleCount.containskey(std.Class__c)) {
                        mapClassidtoFemaleCount.put(std.Class__c ,1);
                    } else {
                        mapClassidtoFemaleCount.put(std.Class__c,mapClassidtoFemaleCount.get(std.Class__c) + 1);
                    }
                }
            } else if(!mapClassidtoCount.containskey(std.Class__c)) {
                    mapClassidtoCount.put(std.Class__c,1);
                if(std.Gender__c == 'Female' && std.Gender__c != Null) {
                    if(!mapClassidtoFemaleCount.containskey(std.Class__c)) {
                        mapClassidtoFemaleCount.put(std.Class__c ,1);
                    } else {
                        mapClassidtoFemaleCount.put(std.Class__c,mapClassidtoFemaleCount.get(std.Class__c) + 1);
                    }
               	}
            }
            
            //count ==maxcount-1
            else if(mapClassidtoCount.get(std.Class__c) == (mapClassidtoMax.get(std.Class__c).Max_Count__c - 1)) {
                if(std.Gender__c == 'Male' && (!mapClassidtoFemaleCount.containskey(std.Class__c))) {
                    newTriggerMap.get(std.Id).addError('class should have atleast one female');
                } else if(std.Gender__c == 'Female') {
                        mapClassidtoCount.put(std.Class__c,mapClassidtoCount.get(std.Class__c) + 1);
                        if(!mapClassidtoFemaleCount.containskey(std.Class__c)) {
                        mapClassidtoFemaleCount.put(std.Class__c ,1);
                    	} else {
                        mapClassidtoFemaleCount.put(std.Class__c,mapClassidtoFemaleCount.get(std.Class__c) + 1);
                    }
                } else if(std.Gender__c == 'Male' && mapClassidtoFemaleCount.get(std.Class__c) >= 1) {
                    mapClassidtoCount.put(std.Class__c,mapClassidtoCount.get(std.Class__c) + 1);
                }
                
            } else {
            //count == maxcount
            if(mapClassidtoCount.get(std.Class__c) == mapClassidtoMax.get(std.Class__c).Max_Count__c) {
                newTriggerMap.get(std.Id).addError('Cannot add max limit reached');
            }
            }
            
            
        }
        System.debug('mapClassidtoCount=='+mapClassidtoCount);
        System.debug('mapClassidtoFemaleCount=='+mapClassidtoFemaleCount);
        System.debug('mapClassidtoMax=='+mapClassidtoMax);
        
        List<Class__c> classUpdate = new List<Class__c>();
        for(Id idset : setofclassid) {
            Class__c cls = new Class__c(Id = idset , Count__c = mapClassidtoCount.get(idset));
            classUpdate.add(cls);
        }
        update classUpdate;
      //for tracking  
        
        FieldTracking__c track = new FieldTracking__c(Log__c='');
        if(newTriggerList != Null) {
        for(Student1__c tnlist : newTriggerList) {
               if(oldTriggerList == Null) {
                     track.Log__c += 'INSERT LOG::::::<<<<<<< At ID = ' + tnlist.Id + 'FIRST NAME= '+tnlist.First_Name__c+'LAST NAME= '+tnlist.Last_Name__c+
                        'GENDER= '+tnlist.Gender__c+'EMAIL= '+tnlist.Email__c+'>>>>>>>>';
               } else if(oldTriggerMap != null && newTriggerList != null) {
               if(tnlist.Class__c != oldTriggerMap.get(tnlist.Id).Class__c) {
                   track.Log__c += 'UPDATE LOG:::::<<<<<<<< Class changed from '+ oldTriggerMap.get(tnlist.Id).Class__c+ ' to ' +tnlist.Class__c+'>>>>>>>>>>';
               }
               if(tnlist.First_Name__c != oldTriggerMap.get(tnlist.Id).First_Name__c) {
                    track.Log__c += 'UPDATE LOG:::::<<<<<<< First Name Changed from '+oldTriggerMap.get(tnlist.Id).First_Name__c+' to '+tnlist.First_Name__c+'>>>>>>>>';
               }   
               if(tnlist.Last_Name__c != oldTriggerMap.get(tnlist.Id).Last_Name__c) {
                    track.Log__c += 'UPDATE LOG:::::<<<<<<< Last Name changed from '+oldTriggerMap.get(tnlist.Id).First_Name__c+' to '+tnlist.First_Name__c+ '>>>>>>>>';
               }    
               if(tnlist.Email__c != oldTriggerMap.get(tnlist.Id).Email__c) {
                    track.Log__c += 'UPDATE LOG:::::<<<<<< Email changed from '+oldTriggerMap.get(tnlist.Id).Email__c+' to '+tnlist.Email__c+'>>>>>>>>';
                }
             
           }
        }
        }
        if(newTriggerList == Null && oldTriggerList != Null  ) {
               for(Student1__c tolist : oldTriggerList) {
               track.Log__c += 'DELETE LOG ::::<<<<<<< At ID = ' + tolist.Id + 'FIRST NAME= '+tolist.First_Name__c+'LAST NAME= '+tolist.Last_Name__c+
                    'GENDER= '+tolist.Gender__c+'EMAIL= '+tolist.Email__c+'>>>>>>>>';
           		}
           }
        
        insert track;
        
    
    }
}