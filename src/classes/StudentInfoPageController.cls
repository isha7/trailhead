public class StudentInfoPageController {
    public Student1__c studentRecord{get;set;}
    public Boolean showStudentInfo{get;set;}
    public String rollno{get;set;}
    public Teacher__c teacher{get;set;}
    public Boolean showAppForm{get;set;}
    public List<SelectOption> classList{get;set;}
    public String classId{get;set;}
    public String fname{get;set;}
    public String gender{get;set;}
    public Student1__c newStudent{get;set;}
    
    public StudentInfoPageController() {
       showStudentInfo=false;
       showAppForm = false;
       studentRecord = new Student1__c();
       teacher = new Teacher__c();
        classId='';
       
    }
    public void showInfo() {
        showStudentInfo=false;
        if(rollno != '') {
            studentRecord = [SELECT Name,Gender__c,Class__c,First_Name__c,Class__r.Name FROM Student1__c WHERE Name = :rollno];
            teacher = [SELECT Name FROM Teacher__c WHERE Class__c = :studentRecord.Class__c AND Is_ClassTeacher__c = true];
            showStudentInfo=true;
        }
    }
    public void applicationform() {
        System.debug('debug applicationform');
        classList =new List<SelectOption>();
        for(Class__c c : [Select Name From Class__c Limit 10]) {
            classList.add(new SelectOption(c.Id,c.Name));
        }
        showAppForm = true;
    }
    public PageReference submission() {
        newStudent = new Student1__c(Gender__c = gender , First_Name__c = fname , Class__c = classId);
        insert newStudent;
        PageReference homePage = new PageReference('/apex/StudentInfoPage');  
        homepage.setRedirect(true);
        return homepage;
    }
}