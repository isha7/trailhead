public class VfpageController {
    public List<SelectOption> accountList{get;set;}
    public List<SelectOption> noteList{get;set;}
    public String accountId{get;set;}
    public String noteId{get;set;}
    public Boolean showNoteList{get;set;}
    public Boolean showoutputField{get;set;}
    public String userId;
    public Note__c noteDetail{get;set;}
    public Map<Id,Note__c> maptonote;
    
    public VfpageController() {
        showNoteList = false;
        showoutputField =false;
        maptonote =new Map<Id,Note__c>();
        //user check
        userId = ApexPages.currentPage().getParameters().get('uid');
        if(userId == Null) {
            userId = UserInfo.getUserId();
        }
        accountList = new List<SelectOption>();
        accountList.add(new SelectOption('none','None'));
        for(Account a : [SELECT Name FROM Account WHERE OwnerId = :userId LIMIT 20]) {
            accountList.add(new SelectOption(a.Id,a.Name));
        }
		       
    }
    
    public void createNotes() {
        showNoteList = false;
        showOutputField = false;
        if(accountId != 'none') {
            noteList = new List<SelectOption>();
            noteList.add(new SelectOption('none','None'));
            for(Note__c n : [SELECT Name,LastModifiedDate,CanEdit__c FROM Note__c WHERE Account__c = :accountId]) {
                noteList.add(new SelectOption(n.Id,n.Name));
                maptonote.put(n.Id,n);
            }
            showNoteList = true;
        }
    }
    
    public void createOutputField() {
        showOutputField = false;
        if(noteId != 'none') {
            noteDetail = maptonote.get(noteId);
            showOutputField = true;
        }
    }
}