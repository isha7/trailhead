public class CandidateTriggerClass {
    public void checkDuplicate(List<Candidate__c> triggerNewList){
        Set<String> fname = new Set<String>();
        Set<String> lname = new Set<String>();
        Set<String> email = new Set<String>();
        
        for(Candidate__c tnlist : triggerNewList) {
            if(!fname.contains( tnlist.First_Name__c ) ) {
                fname.add( tnlist.First_Name__c );
            }
            if(!lname.contains( tnlist.Last_Name__c ) ) {
                lname.add( tnlist.Last_Name__c );
            }
            if(!email.contains( tnlist.Email__c ) ) {
                email.add( tnlist.Email__c );
            }           
        }
        
        List<Candidate__c> candidatecombo = [SELECT First_Name__c , Last_Name__c , Email__c FROM Candidate__c 
                                                    WHERE First_Name__c IN :fname 
                                                    AND Last_Name__c IN :lname AND Email__c IN :email];
        
        Map<String, Id> m = new Map<String, Id>();
        
        
        for(Candidate__c c : candidatecombo) {
            String key = (c.First_Name__c + c.Last_Name__c + c.Email__c);
            if(!m.containskey(key)) {
            	m.put(key,c.Id);
            }
        }
        
        for(Candidate__c tlist : triggerNewList){
            String key = (tlist.First_Name__c + tlist.Last_Name__c + tlist.Email__c);
            if(m.containskey(key) && tlist.Id != m.get(key) ) {
                tlist.addError('Can\'t conatin duplicate values');
            } else {
                if(!m.containskey(key)) {
               		m.put(key,tlist.Id);
                }
            }
        }
    }
}