public class TestController {
    public List<SelectOption> accountList{get;set;}
    public List<SelectOption> contactList{get;set;}
    public String UserId;
    public Boolean showButton{get;set;}
    public String accountId{get;set;}
    public String contactId{get;set;}
    public Boolean showContact{get;set;}
    public Boolean showContactDetail{get;set;}
    public Boolean showInputField{get;set;}
    public Boolean showOutputField{get;set;}
    public Boolean showSaveButton{get;set;}
    public MAP<id,Contact> contactIdMap;
    public Contact contactDetail{get;set;}
    public TestController() {
        accountId='';
        contactId='';
        UserId = ApexPages.currentPage().getParameters().get('uid');
        if(UserId == Null) {
            UserId = UserInfo.getUserId();
        }
        showContact =false;
        showInputField = false;
        showSaveButton = false;
        showOutputField = true;
        showContactDetail=true;
        showButton = false;
        
        accountList = new List<SelectOption>();
        contactIdMap = new MAP<id,Contact>();
        accountList.add(new SelectOption('none','None'));
        for(Account a : [SELECT Name FROM Account WHERE OwnerId = :UserId LIMIT 20]) {
            accountList.add(new SelectOption(a.Id,a.Name));
        }
    }
    
    public void createcontacts() {
        contactList = new List<SelectOption>();
        contactList.add(new SelectOption('none','None'));
        showContactDetail = false;
        showButton=false;
        showSaveButton = false;
        if(accountId != 'none') {
            for(Contact c : [SELECT FirstName,LastName,Name, Phone, Email, Department,CanEdit__c FROM Contact WHERE AccountId = :accountId]){
                contactList.add(new SelectOption(c.Id,c.Name));
                contactIdMap.put(c.id, c);
            }
             showContact =true;
        } else {
            showContact = false;
            showContactDetail=false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'You need to Choose Valid Account Name'));
        
            
        }
        if(contactList.size() == 1 && accountId != 'none'){
            showContact = false;
            showContactDetail = false;
            showInputField = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,'No Contact Related to it'));
        }
        
    }
    public void createDetail() {
        showButton=false;
        if(contactId != 'none') {
            contactDetail = contactIdMap.get(contactId);
            if(contactIdMap.get(contactId).CanEdit__c == true) {
                showButton = true;
            } else {
                showButton = false;
            }
            showContactDetail = true;
        } else {
            showContactDetail = false;
        }
    }
    public void clicking() {
            showOutputField = false;
            showInputField = true;
        	showSaveButton = true;
        	showButton = false;
        	
              
     }
    public void saving() {
        update contactDetail;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,'Successfully Edited at Contact Id : '+contactId));
        showButton = true;
        showSaveButton = false;
        showOutputField = true;
        showInputField = false;
    }
}