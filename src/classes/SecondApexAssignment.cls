public class SecondApexAssignment {
    //to get next working day
    public static Date getNextWorkingdayDate(Date dateparameter) {
        DateTime getday = DateTime.newInstance(dateparameter.year() , dateparameter.month() , dateparameter.day());
        String day = getday.format('EEEE');
        System.debug(day);
        if(day == 'Saturday') {
            System.debug('if saturdaay ==' + (dateparameter + 2));
            return dateparameter + 2 ;
        } else if(day == 'Friday') {
            System.debug('if friday ==' + (dateparameter + 3));
            return dateparameter + 3 ;
        } else {
            System.debug('else ==' + (dateparameter + 1));
            return dateparameter + 1 ;
        }
    }
    
    //takes class id as an argument and return names of teachers associated with a class
    public static List<Teacher__c> getTeacher(Id classid) {
        List<Teacher__c> teachers =new List<Teacher__c>([SELECT Name FROM Teacher__c WHERE Class__c = :classid]);
        System.debug(teachers);
        return teachers;
    }
}