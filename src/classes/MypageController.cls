public class MypageController {
    public String accountId{get;set;}
    public List<SelectOption> accountList{get;set;}
    public String UserId;
    public String cid;
    public String confirmmsg;
    public Boolean showContact{get;set;}
    public String contactId{get;set;}
    public Contact contactDetail{get;set;}
    public static Boolean showOutputForm{get;set;}
    public Boolean showOutputField{get;set;}
    public Boolean showInputField{get;set;}
    public Boolean showEditButton{get;set;}
    public List<SelectOption> contactList{get;set;}
    public Map<Id,Contact> contactIdMap;
    public MypageController() {
        cid = ApexPages.currentPage().getParameters().get('cid');
        confirmmsg = ApexPages.currentPage().getParameters().get('confirmmsg');
        if(cid != Null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,confirmmsg+' at Contact Id : '+cid ));
        }
        accountId='';
        contactId='';
        showOutputField = true;
        showInputField = false;
        showContact = false;
        showEditButton = false;
        showOutputForm = false;
        contactIdMap = new Map<Id,Contact>();
        UserId = ApexPages.currentPage().getParameters().get('uid');
        if(UserId == Null) {
            UserId = UserInfo.getUserId();
        }
        accountList = new List<SelectOption>();
        accountList.add(new SelectOption('none','None'));
        for(Account a : [SELECT Name FROM Account WHERE OwnerId = :UserId LIMIT 20]) {
            accountList.add(new SelectOption(a.Id,a.Name));
        }
        
    }
    
    public void createContact() {
        showContact = false;
        showOutputForm =false;
        showEditButton = false;
        contactList = new List<SelectOption>();
        contactList.add(new SelectOption('none','None'));
        if(accountId != 'none') {
            for(Contact c : [SELECT Name,Phone,Department,CanEdit__c,Email FROM Contact WHERE AccountID = :accountId]) {
                contactList.add(new SelectOption(c.Id,c.Name));
                contactIdMap.put(c.Id,c);
            }
            showContact = true;
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'You need to Choose Valid Account Name'));
        }
        if(contactList.size() == 1 || accountId == 'none') {
            showContact = false;
            accountnonecase();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,'No Contact Related to it'));
        }
    }
    
    public void createOutputField() {
        showOutputForm =false;
        showEditButton = false;
        if(contactId != 'none') {
            contactDetail = contactIdMap.get(contactId);
            showOutputForm = true;
            if(showInputField == true) {
            	showOutputForm = false;
            }
        }
        if(contactId != 'none' && contactIdMap.get(contactId).CanEdit__c == True) {
            showEditButton = true;
        }
       
    }
    
    public void accountnonecase() {
         if(accountId == 'none' && showContact == false) {
            showOutputForm =false;
            showEditButton = false;
            showInputField =false;
        }
    }
    
    public PageReference clickingEdit() {
        showOutputField = false;
        showInputField =true;
        PageReference editpage = new PageReference('/apex/MyEditPage?cid='+contactId);
        editpage.setRedirect(true);
        return editpage;
     
    }
}