public class Example2PageController {
 public String name { get; set; }
 public Integer count { get;set; }
  public Example2PageController(){
    name = UserInfo.getName(); 
    count = 10;  
  }
 
  public void startCounting(){  
    name = UserInfo.getName()+' after ' + count + ' seconds';  
    count += 10; 
  }
}