public class Getstudent {
    //takes classId as an argument and returns names of the students in that class as a list
    public static List<Student1__c> noOfStudent(Id classid){
        List<Student1__c> st = new List<Student1__c>([Select First_Name__c , Last_Name__c , Name from Student1__c where Class__c = :classid]);
        return st;
    }
    
    //takes two arguments and return number of working days between two dates Excluding Saturday and Sunday
    public static Integer workingDays(Date d1 , Date d2) {
        Integer n = d1.daysBetween(d2);
        System.debug(n);
        Integer d = 0;
        Date a = d1;
        for(Integer i = 1 ; i <= n ; i++) {
            Datetime currentdate = Datetime.newInstance(a.year() , a.month() , a.day());
            String currentday = currentdate.format('EEEE');
            if(currentday != 'Sunday' && currentday != 'Saturday') {
                d += 1;
                
            }
            a = a.addDays(1);
        }
        System.debug(d);
        return d;
    }
    
    //shows list of sobjects exist in organization
    public static void getSobject() {
        Map<String, Schema.SObjectType> n = Schema.getGlobalDescribe();
        System.debug(n.keySet());
        System.debug(n.size());
    }
    //returns a MAP where key is class id and value is set of students in it.
    public static Map<Id,Set<Student1__c>> getMapofClassStudent() {
        List<Class__c> classlist = [SELECT Id FROM Class__c]; //ask 
        List<Student1__c> listofstudent = new List<Student1__c>([SELECT Name , Class__c FROM Student1__c WHERE Class__c IN :classlist]);
        Set<Student1__c> sets = new Set<Student1__c>();
        
        Map<Id,Set<Student1__c>> mapofClassIdStudentRecord = new Map<Id,Set<Student1__c>>();
        
        for(Student1__c setoflist: listofstudent) {
            sets.add(setoflist);
        }
        
        for(Student1__c getset : sets) {
            if(!mapofClassIdStudentRecord.containskey(getset.Class__c)){
           		 mapofClassIdStudentRecord.put(getset.Class__c , new Set<Student1__c>());
            }
            mapofClassIdStudentRecord.get(getset.Class__c).add(getset);
        }
        System.debug(mapofClassIdStudentRecord);
        return mapofClassIdStudentRecord;
    }
    //return Map which key as Student Id and Value as Class teacher Name.
    public static Map<Id,Teacher__c> getStudentidClassTeacher() {
        Map<Id,Teacher__c> maptogetclassteacher = new Map<Id,Teacher__c>();
        List<Teacher__c> teacherlist = new List<Teacher__c>([SELECT Class__c , Name FROM Teacher__c WHERE Is_ClassTeacher__c = True]);
        Set<Id> classidset = new Set<Id>();
        for(Teacher__c teachlist : teacherlist) {
            if(!maptogetclassteacher.containskey(teachlist.Class__c)) {
                maptogetclassteacher.put(teachlist.Class__c,teachlist);
            }
        }
        System.debug('maptogetclassteacher==='+ maptogetclassteacher);
        Map<Id,Teacher__c> maptoStudentTeacher = new Map<Id,Teacher__c>();
        List<Student1__c> studentlist = new List<Student1__c>([SELECT Class__c FROM Student1__c]);
        
        for(Student1__c slist : studentlist) {
            if(maptogetclassteacher.containskey(slist.Class__c)) {
                maptoStudentTeacher.put(slist.Id , maptogetclassteacher.get(slist.Class__c));
            }
        }
        System.debug('maptoStudentTeacher===='+maptoStudentTeacher);
        return maptoStudentTeacher;
    }
    
    //mail to Class teacher if student record is not modified for last 30Days.
    public static void mailTeacher() {
        List<Teacher__c> teachers = new List<Teacher__c>([SELECT Class__c ,Email__c FROM Teacher__c WHERE Is_ClassTeacher__c = True]);
        Map<Id,Teacher__c> mapclasstoteacher = new Map<Id,Teacher__c>();
        for(Teacher__c teach : teachers) {
            if(!mapclasstoteacher.containskey(teach.Class__c)) {
                mapclasstoteacher.put(teach.Class__c , teach);
            }
        }
        System.debug('mapclasstoteacher=='+mapclasstoteacher);
        Date todaysdate = Date.today();
        System.debug(todaysdate);
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        List<Student1__c> students = new List<Student1__c>([SELECT Class__c ,LastModifiedDate,Name FROM Student1__c WHERE Class__c IN :mapclasstoteacher.keyset()]);
        for(Student1__c slist : students) {
            Date modifieddate = slist.LastModifiedDate.date();
            System.debug(modifieddate.daysBetween(todaysdate));
            System.debug('slist.Class__c=='+slist.Class__c + 'mapclasstoteacher.get(slist.Class__c).Class__c=='+mapclasstoteacher.get(slist.Class__c).Class__c);
            
            if(slist.Class__c == mapclasstoteacher.get(slist.Class__c).Class__c && mapclasstoteacher.get(slist.Class__c).Email__c != Null) {
               //if modified date greater than 30 days 
                if(modifieddate.daysBetween(todaysdate) > 3) {
                    Messaging.SingleEmailMessage mail = EmailMessanger.sendingmail(mapclasstoteacher.get(slist.Class__c).Email__c , 'Record of ' + slist.Name+'has not been modified more than 30 days','subject');
                    mails.add(mail);
                }
            //if record modified today
                if(modifieddate == todaysdate) {
                    Messaging.SingleEmailMessage mail = EmailMessanger.sendingmail(mapclasstoteacher.get(slist.Class__c).Email__c , 'record modified today','subject');
                    mails.add(mail);
                }
        	}
        }
        Messaging.sendEmail(mails);
    }
  
    
}