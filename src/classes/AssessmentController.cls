public class AssessmentController {
    public String id{get;set;}
    public String userid{get;set;}
    public String opportunityId;
    public String accountId;
    public String contactId;
    public Boolean show{get;set;}
	public Boolean dontshow{get;set;}   
    
    public String noteName{get;set;}
    public String noteDescription{get;set;}
    public String noteDate{get;set;}
    public String noteRecordOwner{get;set;}
    public Boolean showAccount{get;set;}
    public Boolean showContact{get;set;}
    public Boolean showOpportunity{get;set;}
    public Contact contactRecord{get;set;}
    public Opportunity opportunityRecord{get;set;}
    public Account accountRecord{get;set;}
    public Boolean showConRecord{get;set;}
    public Boolean showAccRecord{get;set;}
    public Boolean showOppRecord{get;set;}
    public Note__c noteRecord{get;set;}
    public List<SelectOption> lookupList{get;set;}
    public String lookupId{get;set;}
    public Boolean showLookupList{get;set;}
    public MAP<id,Contact> contactIdMap;
    public MAP<id,Account> accountIdMap;
    public MAP<id,Opportunity> opportunityIdMap;
    
    public AssessmentController() {
        show=true;
        id = ApexPages.currentPage().getParameters().get('id');
        userId = ApexPages.currentPage().getParameters().get('userid');
        if(userId == Null) {
            userId = UserInfo.getUserId();
        }
        if(id.contains('003')) {
            contactId = id;
            contactRecord =new Contact();
            contactRecord = [SELECT Name,Email FROM Contact WHERE Id = :contactId];
            noteRecord = new Note__c();
            noteRecord = [SELECT Name,Description__c,Date__c,Record_Owner__c FROM Note__c WHERE Contact__c = :contactRecord.Id];
            showContact = true;
        } else if(id.contains('001')) {
            accountId = id;
            accountRecord = new Account();
            accountRecord = [SELECT Name,BillingAddress FROM Account WHERE Id = :accountId];
            noteRecord = new Note__c();
            noteRecord = [SELECT Name,Description__c,Date__c,Record_Owner__c FROM Note__c WHERE Account__c = :accountRecord.Id];
          
            showAccount = true;
        } else if(id.contains('006')) {
            opportunityId = id;
            System.debug(opportunityId);
            opportunityRecord = new Opportunity();
            opportunityRecord = [SELECT Name,CloseDate FROM Opportunity WHERE Id = :opportunityId];
            noteRecord = new Note__c();
            noteRecord = [SELECT Name,Description__c,Date__c,Record_Owner__c FROM Note__c WHERE Opportunity__c = :opportunityRecord.Id];
           
            showOpportunity = true;
        }   
    }
    public void callField() {
        if(id.contains('003')) {
            showConRecord=true;
       		showAccRecord=false;
            showOppRecord=false;
        } else if(id.contains('001')) {
            showConRecord=false;
       		showAccRecord=true;
            showOppRecord=false;
        } else if(id.contains('006')) {
           showConRecord=false;
       		showAccRecord=false;
            showOppRecord=true;
        }
    }
    public void reseting() {
        noteRecord.Description__c = '';
        noteRecord.Date__c = Null;
        noteRecord.Record_Owner__c=Null;
    }
    
    
    public void save(){
        show = true;
        dontshow=false;
        if(noteRecord.Description__c == '' || noteRecord.Date__c == Null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Fill Description and DOB they can\'t be empty'));
        } else {
            //noteRecord = new Note__c(Description__c = noteDescription , Date__c = date.parse(noteDate) , Record_Owner__c = userid);
            update noteRecord;
        
        showLookupList =true;
        lookupList = new List<SelectOption>();
        if(id.contains('003')) {
            for(Contact clist : [SELECT Name FROM Contact WHERE OwnerID = :userId LIMIT 10]) {
                lookupList.add(new SelectOption(clist.Id,clist.Name));
                contactIdMap.put(clist.Id,clist);
            }
           
        } else if(id.contains('001')) {
            for(Account alist : [SELECT Name FROM Account WHERE OwnerID = :userId LIMIT 10]) {
                lookupList.add(new SelectOption(alist.Id,alist.Name));
                accountIdMap.put(alist.Id,alist);
        	}
            
        } else if(id.contains('006')) {
            for(Opportunity olist : [SELECT Name FROM Opportunity WHERE OwnerID = :userId LIMIT 10]) {
                lookupList.add(new SelectOption(olist.Id,olist.Name));
                
        	}
           
        }
            show = false;
        dontshow=true;
            
        }
     
    }
    
	    
    
}