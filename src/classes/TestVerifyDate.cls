@isTest
private class TestVerifyDate {
    @isTest static void checkDateiin30(){
        Date d1 = date.parse('3/14/2016');
        Date d2 = date.parse('3/25/2016');
        Date d = VerifyDate.CheckDates(d1,d2);
        System.assertEquals(d2, d);
        
    }
    @isTest static void checkDatenot30(){
        Date d1 = date.parse('3/1/2016');
        Date d2 = date.parse('4/30/2016');
        Date d3 = date.parse('3/31/2016');
        Date d = VerifyDate.CheckDates(d1,d2);
        System.assertEquals(d3, d);
    }

}