global class NotifyManager implements Database.Batchable<sObject> {
    public Date day;
    public Date firstDay;
    public Date lastDay;
    global NotifyManager() {
      	day = Date.today();
        firstDay = Date.newInstance(day.year(), day.month(), 1);
        lastDay = FirstDay.addMonths(1).toStartofMonth().addDays(-1);
    }	
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT FirstYearCompletionDate__c,Manager__c,Manager__r.Email__c,First_Name__c,Last_Name__c,Last_Promotion_Date__c FROM Employee__c WHERE FirstYearCompletionDate__c >= :firstDay AND FirstYearCompletionDate__c <= :lastDay';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<Employee__c> records) {
        Map<Id,String> mapManagertoEmployee = new Map<Id,String>();
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        String s = '';
        for(Employee__c m : records) {
            if(!mapManagertoEmployee.containsKey(m.Manager__c)) {
                mapManagertoEmployee.put(m.Manager__c,'');
            } else {
            	s=s + ' '+m.First_Name__c +''+m.Last_Name__c+' ';
                mapManagertoEmployee.put(m.Manager__c ,s);
            }
        } 
        for(Employee__c m : records) {
            List<String> toAddresses = new List<String>();           
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            toAddresses.add(m.Manager__r.Email__c);
            mail.setToAddresses(toAddresses);
            mail.setSubject('One Year Completion of Employees');
            String messageBody = 'Hi, Your Employees - ' +mapManagertoEmployee.get(m.Manager__c) + 'has completed one year this month';
            if(m.Last_Promotion_Date__c == Null) {
                messageBody = messageBody + m.First_Name__c + ''+m.Last_Name__c+' has not been promoted yet';
            }
            mail.setHtmlBody(messageBody); 
            mailList.add(mail);
        }
        Messaging.sendEmail(mailList);
    }
    global void finish(Database.BatchableContext bc) {
    }
}