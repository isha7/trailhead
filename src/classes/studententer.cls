public class studententer {
    public static void enterStudent() {
        List<Class__c> clas = new List<Class__c>([Select id from Class__c]);
        List<Student1__c> st = new List<Student1__c>();
        for(Class__c cls : clas){
            for(Integer i=1;i<=3;i++){
                Student1__c stdnt = new Student1__c(Email__c='a'+i+'@mail.com', First_Name__c='student'+i , Last_Name__c = 'last' ,
                                                    Class__c = cls.id);
                st.add(stdnt);
            }
        }
        insert st;
        System.debug('done');
    }
    public static void enterTeacher() {
        List<Class__c> cls = new List<Class__c>([SELECT Id FROM Class__c]);
        List<Teacher__c> t = new List<Teacher__c>();
        for(Class__c clss : cls){
            for(Integer i=1;i<=3;i++){
                Teacher__c tt = new Teacher__c(Email__c='shuklaisha7@gmail.com', Name='teacher' +i ,First_Name__c='first'+i , Last_Name__c = 'lastname' , Class__c = clss.id);
                t.add(tt);
            }
        }
        insert t;

    }

}