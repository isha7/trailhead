public class ContactAndLeadSearch {
    public static List<List< SObject>> searchContactsAndLeads(String s){
        List<List< SObject>> s1 = [Find :s in all fields Returning Contact(firstname,lastname Where (firstname = :s OR lastname = :s)) ,Lead(firstname,lastname Where (firstname = :s OR lastname = :s))];
        return s1;
    }

}