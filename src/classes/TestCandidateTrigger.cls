@isTest
public class TestCandidateTrigger {
    	@isTest static void checkallsame(){
            List<Candidate__c> testlist= CandidateTriggerTestData.testData(5,false);
            testlist[0].First_Name__c = 'bhavesh';
            
            List<Candidate__c> clist = new List<Candidate__c>();
            Candidate__c c= new Candidate__c(First_Name__c = 'bhavesh', Last_Name__c = 'last1',Email__c='test1@email.com');
            
         
        Test.startTest();
            try{
         insert testlist;
         insert c;
            } catch(Exception e) {
                 System.assertEquals(5,[Select id from Candidate__c].size());
            }
        
        Test.stopTest();
       
        }
    
    @isTest static void checkalldifferent() {
        List<Candidate__c> difflist = CandidateTriggerTestData.testData(5, false);
        insert difflist;
        List<Candidate__c> clist = new List<Candidate__c>();
        for(Integer i=1 ; i <= 5 ; i++ ) {
            Candidate__c c = new Candidate__c(First_Name__c = 'testt'+i , Last_Name__c='lastt'+i , Email__c='testt'+i+'@email.com');
            clist.add(c);
        }
        Test.startTest();
        insert clist;
        Test.stopTest();

    }
    
    @isTest static void checkamix(){
        List<Candidate__c> difflist = CandidateTriggerTestData.testData(5, false);
        insert difflist;
        List<Candidate__c> clist = new List<Candidate__c>();
        
        for(Integer i=1 ; i <= 10 ; i++ ) {
            Candidate__c c = new Candidate__c(First_Name__c = 'testt'+i , Last_Name__c='lastt'+i , Email__c='testt'+i+'@email.com');
            clist.add(c);
        }
        Candidate__c c1 = new Candidate__c(First_Name__c='test',Last_Name__c='rr',Email__c='test@test.com');
        Candidate__c c2 = new Candidate__c(First_Name__c='test',Last_Name__c='rr',Email__c='test@test.com');
        clist.add(c1);
        clist.add(c2);
        Test.startTest();
        try{
        	insert clist;
        } catch(Exception e) {
            System.debug('null');
        }
        Test.stopTest();

    }

        
}