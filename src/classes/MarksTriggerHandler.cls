public class MarksTriggerHandler {
    public static void getMarks(List<Marks__c> newTriggerList) {
        Map<Id,Integer> mapContactidtoTotalMarks = new Map<Id,Integer>();
        Set<Id> contactidSet = new Set<Id>();
        Map<Id,Integer> mapContactidtoAverage = new Map<Id,Integer>();
        for(Marks__c tnlist : newTriggerList) {
            if(!contactidSet.contains(tnlist.Contact__c)) {
                contactidSet.add(tnlist.Contact__c);
            }
        }
        for(Id cset : contactidSet) {
           mapContactidtoTotalMarks.put(cset , 0);
        }
        List<Marks__c> markList = new List<Marks__c>([SELECT Subject__c , Marks_Obtained__c , Contact__c FROM Marks__c WHERE Contact__c IN :contactidSet]);
        Integer totalSubjects = markList.size(); 
        Integer temp = 0  ;
        for(Marks__c mlist : markList) {
            if(mapContactidtoTotalMarks.containskey(mlist.Contact__c)) {
               temp = temp + (Integer)mlist.Marks_Obtained__c;
               mapContactidtoTotalMarks.put(mlist.Contact__c,temp);
                }
        }
        System.debug('mapContactidtoTotalMarks=='+mapContactidtoTotalMarks);
        for(Id conId : mapContactidtoTotalMarks.keyset()) {
            mapContactidtoAverage.put(conid,(mapContactidtoTotalMarks.get(conid)/totalsubjects));
        }
        
        List<Contact> contactUpdate = new List<Contact>();
        for(Id idset : contactidSet) {
            Contact contactobj = new Contact(Id = idset , Total_Marks__c = mapContactidtoTotalMarks.get(idset) ,Average_Marks__c = mapContactidtoAverage.get(idset));
            contactUpdate.add(contactobj);
        }
        update contactUpdate;
        
    }
}