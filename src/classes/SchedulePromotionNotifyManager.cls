global class SchedulePromotionNotifyManager implements Schedulable {
    global void execute(SchedulableContext sc) {
        PromotionNotifyManager pnm = new PromotionNotifyManager();
        Database.executeBatch(pnm);
    }
}