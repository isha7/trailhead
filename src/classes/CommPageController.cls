public class CommPageController {
    public double day1{get;set;}
    public double day2{get;set;}
    public double day3{get;set;}
    public string inprogress{get;set;}
    public string nw{get;set;}
    public string follow{get;set;}
    public Marks__c markRecord;
    public Map<String,Double> stringInteger{get;set;}
    public CommPageController(ApexPages.StandardController stdController) {
        stringInteger = new Map<String,Double>();
        markRecord = (Marks__c)stdController.getRecord();
        System.debug('markRecord>>>'+markRecord);
        if(markRecord.Number1__c > 0) {
            day1 = markRecord.Number1__c;
            inprogress = 'In Progress';
            stringInteger.put(inprogress,day1);
            System.debug('stringInteger>>1'+stringInteger);
        }
        if(markRecord.Number2__c > 0) {
            day2 = markRecord.Number2__c;
            nw = 'New';
            stringInteger.put(nw,day2);
            System.debug('stringInteger>>2'+stringInteger);
        }
        if(markRecord.Number3__c > 0) {
            day3 = markRecord.Number3__c;
            follow = 'follow up';
            stringInteger.put(follow,day3);
            System.debug('stringInteger>>3'+stringInteger);
        }
    }
}