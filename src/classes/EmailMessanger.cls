public class EmailMessanger {
    public static Messaging.SingleEmailMessage sendingmail(String mailaddress , String body,String subject) {
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      List<String> sendTo = new List<String>();
      sendTo.add(mailaddress);
      mail.setToAddresses(sendTo);
      mail.setSubject(subject);
      mail.setPlainTextBody(body);
      return mail;
    }
}