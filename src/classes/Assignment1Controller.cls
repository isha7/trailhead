public class Assignment1Controller {
    public String message{get;set;}
    public String accountId{get;set;}
    public List<SelectOption> accountList{get;set;}
    public Assignment1Controller() {
        message = 'Hello';
        accountId='';
        accountList = new List<SelectOption>();
		accountList.add(new SelectOption('','None'));
        for(Account acc : [SELECT Name FROM Account LIMIT 5]) {
            accountList.add(new SelectOption(acc.Id,acc.Name));
        }
        
    }
    

}