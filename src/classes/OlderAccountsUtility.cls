public class OlderAccountsUtility {
    public static void updateOlderAccounts(){
        Account[] olda = [SELECT Id, Description FROM Account ORDER BY CreatedDate ASC LIMIT 5];
        for(Account a : olda){
            a.Description = 'Heritage Account';
        }
        update olda;
    }

}