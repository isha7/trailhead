@isTest
private class Student1TriggerTest {
    @isTest static void insertStudent() {
        Class__c getclass = UtilityStudent1.getclass(5,false);
        insert getclass;
        List<Student1__c> enterstd = new List<Student1__c>();
        Student1__c std1 = new Student1__c(Gender__c = 'Female',Class__c = getclass.id);
        Student1__c std2 = new Student1__c(Gender__c = 'Female',Class__c = getclass.id);
        enterstd.add(std1);
        enterstd.add(std2);
        
        Test.startTest();
        insert enterstd;
        Test.stopTest();
        
        System.assertEquals([SELECT Count__c FROM Class__c WHERE Id=:getclass.Id limit 1].Count__c,2);
    }
    @isTest static void deleteStudent() {
        Class__c getclass = UtilityStudent1.getclass(5,false);
        insert getclass;
        Student1__c std1 = new Student1__c(Gender__c = 'Female',Class__c=getclass.Id);
        insert std1;
        Student1__c std2 = new Student1__c(Gender__c = 'Female',Class__c=getclass.Id);
        insert std2;
        
        Test.startTest();
        delete std1;
        Test.stopTest();
        System.assertEquals(1,[SELECT Count__c FROM Class__c WHERE Id=:getclass.Id].Count__c);
    }
    @isTest static void changeClass() {
        Class__c getclass1 = UtilityStudent1.getclass(5,false);
        insert getclass1;
        Class__c getclass2 = UtilityStudent1.getclass(5,false);
        insert getclass2;
        Student1__c std = new Student1__c(Gender__c = 'Female',Class__c=getclass1.Id);
        insert std;
        std.Class__c = getclass2.Id;
        Test.startTest();
        update std;
        Test.stopTest();
        
        System.assertEquals(0,[SELECT Count__c FROM Class__c WHERE Id=:getclass1.Id].Count__c);
        System.assertEquals(1,[SELECT Count__c FROM Class__c WHERE Id=:getclass2.Id].Count__c);
    }
    @isTest static void checkTrackifinsert() {
        Class__c getclass = UtilityStudent1.getclass(5,false);
        insert getclass;
        List<Student1__c> enterstd = new List<Student1__c>();
        Student1__c std1 = new Student1__c(Gender__c = 'Female',Class__c = getclass.id);
        Student1__c std2 = new Student1__c(Gender__c = 'Female',Class__c = getclass.id);
        enterstd.add(std1);
        enterstd.add(std2);
        FieldTracking__c track = new FieldTracking__c();
        
        Test.startTest();
        insert enterstd;
        Test.stopTest();
        System.assertEquals(1,[SELECT Log__c FROM FieldTracking__c].size());
        
    }
    @isTest static void checktrackupdate() {
        Class__c getclass1 = UtilityStudent1.getclass(5,true);
        Class__c getclass2 = UtilityStudent1.getclass(5,true);
        Student1__c std = new Student1__c(Gender__c='Female',Class__c = getclass1.Id,First_Name__c = 'Isha',Last_Name__c='Shukla',Email__c='shuklaisha7@gmail.com');
        insert std;
        std.Class__c=getclass2.Id;
        std.Gender__c='Male';
        std.First_Name__c='Ishu';
        std.Last_Name__c='Shuklaa';
        std.Email__c='shshsh@sjhda.com';
        Test.startTest();
        update std;
        Test.stopTest();
        
        System.assertEquals(2, [SELECT Log__c FROM FieldTracking__c].size());
    }
    @isTest static void checkMaxcount() {
        Class__c getclass = UtilityStudent1.getclass(2,false);
        insert getclass;
        List<Student1__c> stdlist = new List<Student1__c>(UtilityStudent1.getstudent(3,'Female',getclass.Id,false));
        Test.startTest();
        try{
       insert stdlist ;
        } catch(Exception e) {
            System.debug(e.getMessage());
        }
        Test.stopTest();
       
       Class__c count = [SELECT Count__c FROM Class__c WHERE Id=:getclass.Id];
       System.assertEquals(2,count.Count__c);
    } 
}