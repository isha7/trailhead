<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Vehicle__c</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>Garage</label>
    <tab>standard-Chatter</tab>
    <tab>standard-File</tab>
    <tab>Vehicle__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Position__c</tab>
    <tab>Job_Application__c</tab>
    <tab>Employement_Website__c</tab>
    <tab>Class__c</tab>
    <tab>Student1__c</tab>
    <tab>Teacher__c</tab>
    <tab>FieldTracking__c</tab>
    <tab>Marks__c</tab>
    <tab>Loan__c</tab>
    <tab>Warehouse__c</tab>
    <tab>Note__c</tab>
    <tab>Employee__c</tab>
    <tab>Expense__c</tab>
    <tab>Camping_Item__c</tab>
</CustomApplication>
